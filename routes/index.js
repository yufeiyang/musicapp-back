let express = require("express")
let router = express.Router()

/* GET home page. */
router.get("/", function(req, res) {
  res.render("index", { title: "musicApp Version 1.0 " })
})

module.exports = router
